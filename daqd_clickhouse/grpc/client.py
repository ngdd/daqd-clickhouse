"""Simple gRPC client based off of ClickHouse's clickhouse_grpc_client.py

"""

import uuid

import grpc

from . import clickhouse_grpc_pb2
from . import clickhouse_grpc_pb2_grpc


DEFAULT_HOST = "localhost"
DEFAULT_PORT = 9100
DEFAULT_USER_NAME = "default"
DEFAULT_ENCODING = "utf-8"


class ClickHouseGRPCError(Exception):
    pass


class ClickHouseGRPCClient:
    def __init__(
        self,
        host=DEFAULT_HOST,
        port=DEFAULT_PORT,
        user_name=DEFAULT_USER_NAME,
        password="",
        database="",
        output_format="ArrowStream",
        settings="",
    ):
        self.host = host
        self.port = port
        self.user_name = user_name
        self.password = password
        self.database = database
        self.output_format = output_format
        self.settings = settings
        self.channel = None
        self.stub = None
        self.session_id = None

    def __enter__(self):
        self.__connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.__disconnect()

    # Executes a simple query and returns its output.
    def get_simple_query_output(self, query_text):
        result = self.stub.ExecuteQuery(
            clickhouse_grpc_pb2.QueryInfo(
                query=query_text,
                user_name=self.user_name,
                password=self.password,
                database=self.database,
                output_format=self.output_format,
                settings=self.settings,
                session_id=self.session_id,
                query_id=str(uuid.uuid4()),
            )
        )
        ClickHouseGRPCClient.__check_no_errors(result)
        return result.output.decode(DEFAULT_ENCODING)

    # Executes a query using the stream IO.
    def run_query(self, query_text):
        try:

            def send_query_info():
                # send main query info
                info = clickhouse_grpc_pb2.QueryInfo(
                    query=query_text,
                    user_name=self.user_name,
                    password=self.password,
                    database=self.database,
                    output_format=self.output_format,
                    settings=self.settings,
                    session_id=self.session_id,
                    query_id=str(uuid.uuid4()),
                )
                yield info

            for result in self.stub.ExecuteQueryWithStreamIO(send_query_info()):
                ClickHouseGRPCClient.__check_no_errors(result)
                yield result.output

        except Exception:
            raise

    # Establish connection.
    def __connect(self):
        print(
            "Connecting to {host}:{port} as user {user_name}.".format(
                host=self.host, port=self.port, user_name=self.user_name
            )
        )
        # Secure channels are supported by server but not supported by this client.
        self.channel = grpc.insecure_channel(self.host + ":" + str(self.port))
        connection_time = 0
        timeout = 5
        while True:
            try:
                grpc.channel_ready_future(self.channel).result(timeout=timeout)
                break
            except grpc.FutureTimeoutError:
                connection_time += timeout
                print(
                    "Couldn't connect to ClickHouse server in {connection_time} seconds.".format(
                        connection_time=connection_time
                    )
                )
        self.stub = clickhouse_grpc_pb2_grpc.ClickHouseStub(self.channel)

    def __disconnect(self):
        if self.channel:
            self.channel.close()
        self.channel = None
        self.stub = None
        self.session_id = None

    @staticmethod
    def __check_no_errors(result):
        if result.HasField("exception"):
            raise ClickHouseGRPCError(result.exception.display_text)
