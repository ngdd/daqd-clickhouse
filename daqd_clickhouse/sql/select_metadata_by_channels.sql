SELECT
    channel,
    data_type,
    sample_rate
FROM timeseries.`arrakis-metadata` AS metadata
INNER JOIN
(
    SELECT
        channel,
        max(time) AS max_time
    FROM timeseries.`arrakis-metadata`
    PREWHERE
        {% for channel in channels %}
        (channel = '{{ channel }}'){{ " OR" if not loop.last else "" }}
	    {% endfor %}
    GROUP BY channel
) AS grouped_metadata
ON metadata.channel = grouped_metadata.channel
AND metadata.time = grouped_metadata.max_time
