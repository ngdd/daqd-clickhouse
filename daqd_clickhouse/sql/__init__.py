# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/daqd-clickhouse/-/raw/main/LICENSE

from __future__ import annotations

from collections import defaultdict
from dataclasses import dataclass
from typing import Iterable, Optional

import jinja2
import numpy

from arrakis import Channel


@dataclass(frozen=True, order=True)
class ChannelTableId:
    detector: str
    subsystem: str
    data_type: str

    def __str__(self):
        return f"{self.detector}_{self.subsystem}_{self.data_type}"


def get_sql_template(template: str) -> jinja2.environment.Template:
    """Retrieve a SQL template by name"""
    template_loader = jinja2.PackageLoader("daqd_clickhouse", "sql")
    template_env = jinja2.Environment(loader=template_loader, trim_blocks=True)
    return template_env.get_template(template)


def select_data_by_channels(
    channels: Iterable[Channel], start: Optional[int] = None, end: Optional[int] = None
) -> str:
    """Generate SQL query for data based on channels requested"""
    # group channels by table identifier
    channel_groups = defaultdict(list)
    for channel in channels:
        table_id = ChannelTableId(
            channel.detector, channel.subsystem, numpy.dtype(channel.data_type).name
        )
        channel_groups[table_id].append(str(channel))
    table_ids = list(channel_groups.keys())

    # generate SQL for query
    template = get_sql_template("select_data_by_channels.sql")
    return template.render(
        channels=channels,
        table_ids=table_ids,
        channel_groups=channel_groups,
        start=start,
        end=end,
    )


def select_metadata_by_channels(channels: Iterable[Channel]) -> str:
    """Generate SQL query for metadata based on channels requested"""
    template = get_sql_template("select_metadata_by_channels.sql")
    return template.render(channels=channels)


def select_metadata_by_pattern(
    *, pattern: str, data_type: str, min_rate: Optional[int], max_rate: Optional[int]
) -> str:
    """Generate SQL query for metadata based on pattern"""
    template = get_sql_template("select_metadata_by_pattern.sql")
    return template.render(pattern=pattern)


def count_metadata_by_pattern(
    *, pattern: str, data_type: str, min_rate: Optional[int], max_rate: Optional[int]
) -> str:
    """Generate SQL query for metadata count based on pattern"""
    template = get_sql_template("count_metadata_by_pattern.sql")
    return template.render(pattern=pattern)
