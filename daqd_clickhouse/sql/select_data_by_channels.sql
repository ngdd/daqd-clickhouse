{% set ref_table_id = table_ids[0] %}
SELECT
    time,
    {% for channel in channels %}
    `{{ channel }}`{{ "," if not loop.last else "" }}
    {% endfor %}
FROM
{% for table_id in table_ids %}
{% set channel_group = channel_groups[table_id] %}
{% if table_id != ref_table_id %}
INNER JOIN
{% endif %}
(
    SELECT
        time,
        {% for channel in channel_group %}
        anyIf(data, channel = '{{ channel }}') AS `{{ channel }}`{{ "," if not loop.last else "" }}
        {% endfor %}
    FROM timeseries.`arrakis-{{ table_id.detector }}-{{ table_id.subsystem }}-{{ table_id.data_type }}`
    PREWHERE
	    {% for channel in channel_group %}
        (channel = '{{ channel }}'){{ " OR" if not loop.last else "" }}
		{% endfor %}
    GROUP BY time
) AS {{ table_id }}
{% if table_id != ref_table_id %}
ON {{ table_id }}.time = {{ ref_table_id }}.time
{% endif %}
{% endfor %}
{% if start and end %}
WHERE {{ ref_table_id }}.time >= {{ start }} AND {{ ref_table_id }}.time < {{ end }}
{% elif start %}
WHERE {{ ref_table_id }}.time >= {{ start }}
{% elif end %}
WHERE {{ ref_table_id }}.time < {{ end }}
{% endif %}
ORDER BY time ASC;
