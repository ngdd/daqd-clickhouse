SELECT
    COUNT(*) as "count"
FROM timeseries.`arrakis-metadata` AS metadata
INNER JOIN
(
    SELECT
        channel,
        max(time) AS max_time
    FROM timeseries.`arrakis-metadata`
    PREWHERE
	    match(channel, '{{ pattern }}')
    GROUP BY channel
) AS grouped_metadata
ON metadata.channel = grouped_metadata.channel
AND metadata.time = grouped_metadata.max_time
