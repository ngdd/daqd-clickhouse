CREATE TABLE IF NOT EXISTS timeseries."arrakis-{detector}-{subsystem}-metadata-kafka" (
  time Int64,
  channel LowCardinality(String),
  detector LowCardinality(String),
  subsystem LowCardinality(String),
  identifier LowCardinality(String),
  subidentifier LowCardinality(String),
  data_type LowCardinality(String),
  sample_rate UInt32
  ) ENGINE = Kafka SETTINGS kafka_broker_list = '{broker_url}',
                            kafka_topic_list = 'arrakis-{detector}-{subsystem}-metadata',
                            kafka_group_name = 'arrakis-clickhouse',
                            kafka_poll_max_batch_size = 1,
                            kafka_format = 'JSONEachRow';
