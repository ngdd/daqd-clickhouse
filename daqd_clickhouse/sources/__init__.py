# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/daqd-clickhouse/-/raw/main/LICENSE

import os


def create_kafka_data_table(
    broker_url: str, detector: str, subsystem: str, dtype: str
) -> str:
    # read in SQL
    source_path = os.path.join(os.path.dirname(__file__), "kafka_table_data.sql")
    with open(source_path, "r") as f:
        source_sql = f.read()

    # compose and return
    return source_sql.format(
        broker_url=broker_url,
        detector=detector,
        subsystem=subsystem,
        dtype=dtype,
        clickhouse_dtype=dtype_to_clickhouse_type(dtype),
    )


def create_kafka_data_consumer_view(detector: str, subsystem: str, dtype: str) -> str:
    # read in SQL
    source_path = os.path.join(os.path.dirname(__file__), "kafka_consumer_data.sql")
    with open(source_path, "r") as f:
        source_sql = f.read()

    # compose and return
    return source_sql.format(
        detector=detector,
        subsystem=subsystem,
        dtype=dtype,
    )


def create_source_data_table(detector: str, subsystem: str, dtype: str) -> str:
    # read in SQL
    source_path = os.path.join(os.path.dirname(__file__), "source_table_data.sql")
    with open(source_path, "r") as f:
        source_sql = f.read()

    # compose and return
    return source_sql.format(
        detector=detector,
        subsystem=subsystem,
        dtype=dtype,
        clickhouse_dtype=dtype_to_clickhouse_type(dtype),
    )


def create_kafka_metadata_table(broker_url: str, detector: str, subsystem: str) -> str:
    # read in SQL
    source_path = os.path.join(os.path.dirname(__file__), "kafka_table_metadata.sql")
    with open(source_path, "r") as f:
        source_sql = f.read()

    # compose and return
    return source_sql.format(
        broker_url=broker_url,
        detector=detector,
        subsystem=subsystem,
    )


def create_kafka_metadata_consumer_view(detector: str, subsystem: str) -> str:
    # read in SQL
    source_path = os.path.join(os.path.dirname(__file__), "kafka_consumer_metadata.sql")
    with open(source_path, "r") as f:
        source_sql = f.read()

    # compose and return
    return source_sql.format(
        detector=detector,
        subsystem=subsystem,
    )


def create_source_metadata_table() -> str:
    # read in SQL
    source_path = os.path.join(os.path.dirname(__file__), "source_table_metadata.sql")
    with open(source_path, "r") as f:
        return f.read()


def dtype_to_clickhouse_type(dtype: str) -> str:
    """Returns the clickhouse type given a dtype."""
    _dtype_to_clickhouse = {
        "int16": "Int16",
        "int32": "Int32",
        "int64": "Int64",
        "int128": "Int128",
        "uint16": "UInt16",
        "uint32": "UInt32",
        "uint64": "UInt64",
        "uint128": "UInt128",
        "float32": "Float32",
        "float64": "Float64",
    }
    return _dtype_to_clickhouse[dtype]
