CREATE MATERIALIZED VIEW IF NOT EXISTS timeseries."arrakis-{detector}-{subsystem}-{dtype}-consumer" TO timeseries."arrakis-{detector}-{subsystem}-{dtype}"
  AS SELECT *
  FROM timeseries."arrakis-{detector}-{subsystem}-{dtype}-kafka";
