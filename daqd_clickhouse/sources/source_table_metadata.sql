CREATE TABLE IF NOT EXISTS timeseries."arrakis-metadata" (
  time Int64 CODEC(Delta, LZ4),
  channel LowCardinality(String),
  detector LowCardinality(String),
  subsystem LowCardinality(String),
  identifier LowCardinality(String),
  subidentifier LowCardinality(String),
  data_type LowCardinality(String),
  sample_rate UInt32
  ) ENGINE = MergeTree()
ORDER BY (channel, time)
PRIMARY KEY (channel, time)
TTL FROM_UNIXTIME(toInt32(time * 1e-9) + 315964800) TO VOLUME 'hot',
    FROM_UNIXTIME(toInt32(time * 1e-9) + 315964800) + INTERVAL 1 MONTH TO VOLUME 'cold';
