CREATE TABLE IF NOT EXISTS timeseries."arrakis-{detector}-{subsystem}-{dtype}" (
  time Int64 CODEC(Delta, LZ4),
  channel LowCardinality(String),
  data Array({clickhouse_dtype})
  ) ENGINE = MergeTree()
ORDER BY (channel, time)
PRIMARY KEY (channel, time)
TTL FROM_UNIXTIME(toInt32(time * 1e-9) + 315964800) TO VOLUME 'hot',
    FROM_UNIXTIME(toInt32(time * 1e-9) + 315964800) + INTERVAL 1 DAY TO VOLUME 'cold',
    FROM_UNIXTIME(toInt32(time * 1e-9) + 315964800) + INTERVAL 2 MONTH DELETE;
