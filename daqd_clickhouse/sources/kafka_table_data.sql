CREATE TABLE IF NOT EXISTS timeseries."arrakis-{detector}-{subsystem}-{dtype}-kafka" (
  time Int64,
  channel LowCardinality(String),
  data Array({clickhouse_dtype})
  ) ENGINE = Kafka SETTINGS kafka_broker_list = '{broker_url}',
                            kafka_topic_list = 'arrakis-{detector}-{subsystem}-{dtype}',
                            kafka_group_name = 'arrakis-clickhouse',
                            kafka_poll_max_batch_size = 1,
                            kafka_format = 'ArrowStream';
