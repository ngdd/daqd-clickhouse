CREATE MATERIALIZED VIEW IF NOT EXISTS timeseries."arrakis-{detector}-{subsystem}-metadata-consumer" TO timeseries."arrakis-metadata"
  AS SELECT *
  FROM timeseries."arrakis-{detector}-{subsystem}-metadata-kafka";
