# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/daqd-clickhouse/-/raw/main/LICENSE

import argparse
import itertools

from clickhouse_driver import Client

from . import sources


def create_data_tables(
    database_url: str, kafka_url: str, detector: str, subsystem: str, dtype: str
):
    with Client(database_url) as client:
        # create table with Kafka engine
        query = sources.create_kafka_data_table(kafka_url, detector, subsystem, dtype)
        client.execute(query)

        # create source table
        query = sources.create_source_data_table(detector, subsystem, dtype)
        client.execute(query)

        # create materialized view to consume
        # from Kafka and populate source table
        query = sources.create_kafka_data_consumer_view(detector, subsystem, dtype)
        client.execute(query)


def create_metadata_tables(
    database_url: str, kafka_url: str, detector: str, subsystem: str
):
    with Client(database_url) as client:
        # create table with Kafka engine
        query = sources.create_kafka_metadata_table(kafka_url, detector, subsystem)
        client.execute(query)

        # create source table
        query = sources.create_source_metadata_table()
        client.execute(query)

        # create materialized view to consume
        # from Kafka and populate source table
        query = sources.create_kafka_metadata_consumer_view(detector, subsystem)
        client.execute(query)


def create_database(database_url: str):
    with Client(database_url) as client:
        client.execute("CREATE DATABASE IF NOT EXISTS timeseries;")


def drop_data_tables(database_url: str, detector: str, subsystem: str, dtype: str):
    with Client(database_url) as client:
        # drop tables and views in reverse order
        client.execute(
            f'DROP TABLE IF EXISTS timeseries."arrakis-{detector}-{subsystem}-{dtype}-consumer";'
        )
        client.execute(
            f'DROP TABLE IF EXISTS timeseries."arrakis-{detector}-{subsystem}-{dtype}";'
        )
        client.execute(
            f'DROP TABLE IF EXISTS timeseries."arrakis-{detector}-{subsystem}-{dtype}-kafka";'
        )


def drop_metadata_tables(database_url: str, detector: str, subsystem: str):
    with Client(database_url) as client:
        # drop tables and views in reverse order
        client.execute(
            f'DROP TABLE IF EXISTS timeseries."arrakis-{detector}-{subsystem}-metadata-consumer";'
        )
        # client.execute(f'DROP TABLE IF EXISTS timeseries."arrakis-metadata";')
        client.execute(
            f'DROP TABLE IF EXISTS timeseries."arrakis-{detector}-{subsystem}-metadata-kafka";'
        )


def create_database_cmd():
    parser = argparse.ArgumentParser(prog="daqd-clickhouse-admin-create-database")
    parser.add_argument("url", metavar="URL", help="ClickHouse instance to connect to")

    args = parser.parse_args()

    # create the database
    create_database(args.url)


def create_tables_cmd():
    parser = argparse.ArgumentParser(prog="daqd-clickhouse-admin-create-tables")

    parser.add_argument("url", metavar="URL", help="ClickHouse instance to connect to")
    parser.add_argument("-k", "--kafka-url", help="Kafka broker to connect to")
    parser.add_argument(
        "-d",
        "--detector",
        action="append",
        default=[],
        help="detectors to create tables for",
    )
    parser.add_argument(
        "-s",
        "--subsystem",
        action="append",
        default=[],
        help="subsystems to create tables for",
    )
    parser.add_argument(
        "-t",
        "--dtype",
        action="append",
        default=[],
        help="data types to create tables for",
    )

    args = parser.parse_args()

    # create views
    for detector, subsystem in itertools.product(args.detector, args.subsystem):
        create_metadata_tables(args.url, args.kafka_url, detector, subsystem)
    for detector, subsystem, dtype in itertools.product(
        args.detector, args.subsystem, args.dtype
    ):
        create_data_tables(args.url, args.kafka_url, detector, subsystem, dtype)


def drop_tables_cmd():
    parser = argparse.ArgumentParser(prog="daqd-clickhouse-admin-drop-tables")

    parser.add_argument("url", metavar="URL", help="ClickHouse instance to connect to")
    parser.add_argument(
        "-d",
        "--detector",
        action="append",
        default=[],
        help="detectors to create tables for",
    )
    parser.add_argument(
        "-s",
        "--subsystem",
        action="append",
        default=[],
        help="subsystems to drop tables for",
    )
    parser.add_argument(
        "-t",
        "--dtype",
        action="append",
        default=[],
        help="data types to create tables for",
    )

    args = parser.parse_args()

    # drop views
    for detector, subsystem in itertools.product(args.detector, args.subsystem):
        drop_metadata_tables(args.url, detector, subsystem)
    for detector, subsystem, dtype in itertools.product(
        args.detector, args.subsystem, args.dtype
    ):
        drop_data_tables(args.url, detector, subsystem, dtype)
