# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/daqd-clickhouse/-/raw/main/LICENSE

from __future__ import annotations

from contextlib import contextmanager
from dataclasses import dataclass
from datetime import timedelta
from typing import Iterable, Iterator, Optional

import numpy
import pyarrow

from arrakis import Channel, Time

from .grpc.client import ClickHouseGRPCClient
from . import sql


@contextmanager
def connect(server_url: str) -> Iterator[Connection]:
    """Connect to a ClickHouse instance.

    Parameters
    ----------
    server_url : str
        The ClickHouse instance to connect to.

    Returns
    -------
    Connection
        An open connection to ClickHouse.

    """
    with ClickHouseGRPCClient(host=server_url, database="timeseries") as conn:
        yield Connection(conn)


class Connection:
    def __init__(self, connection: ClickHouseGRPCClient):
        self._conn = connection

    def stream(
        self,
        channels: Iterable[str],
        max_latency: timedelta = timedelta(seconds=1),
    ) -> Stream:
        """Create a stream to process data from.

        Parameters
        ----------
        channels : Iterable[str]
            The channels requested.
        max_latency : timedelta, optional
            The maximum latency to wait for messages from missing channels
            before invoking the `on_gap` policy. Default is 1 second.

        Returns
        -------
        Stream
            A stream to process data from.

        """
        return Stream(self._conn, channels, max_latency=max_latency)


class Stream:
    def __init__(
        self,
        conn: ClickHouseGRPCClient,
        channels: Iterable[str],
        max_latency: timedelta = timedelta(seconds=1),
    ):
        """Create a stream to process data from.

        Parameters
        ----------
        conn : Client
            The database connection.
        channels : Iterable[str]
            The channels requested.
        max_latency : timedelta, optional
            The maximum latency to wait for messages from missing channels
            before invoking the `on_gap` policy. Default is 1 second.

        Returns
        -------
        Stream
            A stream to process data from.

        """
        self._conn = conn
        self._max_latency = max_latency
        self._channels = [Channel.from_name(channel) for channel in channels]
        self._start: Optional[int] = None
        self._end: Optional[int] = None

    def __iter__(self):
        query = sql.select_data_by_channels(
            self._channels, start=self._start, end=self._end
        )
        for row in self._conn.run_query(query):
            yield from self._row_to_buffers(row)
        self._start: Optional[int] = None
        self._end: Optional[int] = None

    def range(
        self,
        start: Optional[int] = None,
        end: Optional[int] = None,
    ) -> Stream:
        """Restrict buffers to fall between start and end times"""
        # trim to start/end
        if start is not None:
            self._start = start
        if end is not None:
            self._end = end
        return self

    def limit(self, n: int) -> Stream:
        """Return up to N buffers"""
        # limit to N
        # FIXME: do the thing
        return self

    def _row_to_buffers(self, row):
        """Format a row into consumer-friendly buffers"""
        if not row:
            return
        try:
            with pyarrow.ipc.open_stream(row) as reader:
                df = reader.read_pandas()
                for _, df_row in df.iterrows():
                    out_buf = {}
                    gps_s, gps_ns = divmod(df_row["time"], Time.SECONDS)
                    for channel, arr in zip(self._channels, df_row[1:]):
                        out_buf[str(channel)] = Buffer(
                            channel=str(channel),
                            seconds=gps_s,
                            nanoseconds=gps_ns,
                            data=numpy.array(arr),
                        )

                    yield out_buf

        except pyarrow.lib.ArrowInvalid:
            pass


@dataclass
class Buffer:
    channel: str
    seconds: int
    nanoseconds: int
    data: numpy.ndarray
